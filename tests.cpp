#include "tests.h"
#include <algorithm>
#include <vector>
#include <cstdint>
#include <iostream>
#include <random>

#include "cpu/cpu_impl.h"

template <typename T>
bool assert_equal(const T& a, const T& b)
{
  if(a.size() != b.size())
    return false;

  for(size_t i = 0; i < a.size(); ++i)
  {
    if(a[i] != b[i])
      return false;
  }

  return true;
}

bool test0_simple_vec()
{
  std::vector<uint32_t> data = {10, 5, 3, 1, 6, 7, 8, 9, 8, 1, 1, 2, 3, 4};

  std::vector<uint32_t> sorted_data(data.size());
  counting_sort(data, sorted_data, *std::max_element(data.begin(), data.end()) + 1);

  std::sort(data.begin(), data.end());

  return assert_equal(data, sorted_data);
}

bool test1_random_vec()
{
  constexpr uint32_t K = 255;

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(0, K);
  std::vector<uint32_t> data(1'000'000);

  for(auto& d : data)
    d = distrib(gen);

  std::vector<uint32_t> sorted_data(data.size());
  counting_sort<std::vector<uint32_t>>(data, sorted_data, K + 1);

  std::sort(data.begin(), data.end());

  return assert_equal(data, sorted_data);
}

bool test2_simple_vec_radix()
{
  std::vector<uint32_t> data = {10, 5, 3, 1, 6, 7, 8, 9, 8, 1, 1, 2, 3, 4};

  std::vector<uint32_t> data2(data);
  radix_sort(data);

  std::sort(data2.begin(), data2.end());

  return assert_equal(data, data2);
}

bool test3_random_vec_radix()
{
  constexpr uint32_t K = 255;

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(0, K);
  std::vector<uint32_t> data(1'000'000);

  for(auto& d : data)
    d = distrib(gen);

  std::vector<uint32_t> data2(data);
  radix_sort(data);

  std::sort(data2.begin(), data2.end());

  return assert_equal(data, data2);
}

bool test4_big_random_vec_radix()
{
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(0, UINT32_MAX);
  std::vector<uint32_t> data(72'000'000);

  for(auto& d : data)
    d = distrib(gen);

  std::vector<uint32_t> data2(data);
  radix_sort(data);

  std::sort(data2.begin(), data2.end());

  return assert_equal(data, data2);
}

bool run_all_tests()
{
  std::vector tests = {
      test0_simple_vec,
      test1_random_vec,
      test2_simple_vec_radix,
      test3_random_vec_radix,
      test4_big_random_vec_radix
  };

  bool res = true;
  for(auto i = 0u; i < tests.size(); ++i)
  {
    auto tmp = tests[i]();
    if(!tmp)
      std::cout << "failed test #" << i << std::endl;
    else
      std::cout << "passed test #" << i << std::endl;
    res = res && tmp;
  }


  return res;
}