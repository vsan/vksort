#include "vk_radix.h"
#include "vk_descriptor_sets.h"
#include <iostream>
#include <array>
#include <cstring>
#include <chrono>


std::vector<uint32_t> ComputeApplication::Run(const std::vector<uint32_t> &numbers, unsigned deviceId,
                                              bool useSubgroups, bool useSimple)
{
  InitVulkan(deviceId, useSubgroups);

  std::cout << "Input size =  " << numbers.size() << std::endl;
  std::cout << "Workgroup size =  " << WORKGROUP_SIZE << std::endl;

  auto cmd = vk_utils::createCommandBuffer(device, commandPoolCompute);

  if(useSubgroups)
  {
    CreateResourcesSubgroups(numbers);
    SubgroupPrefixSumCmd(cmd, numbers.size());
  }
  else
  {
    if(useSimple)
    {
      CreateResourcesSimple(numbers);
      SimplePrefixSumCmd(cmd, numbers.size());
    }
    else
    {
      CreateResourcesStream(numbers);
      StreamPrefixSumCmd(cmd, numbers.size());
    }
  }

  VkFence fence;
  VkFenceCreateInfo fenceCreateInfo = {};
  fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fenceCreateInfo.flags = 0;
  VK_CHECK_RESULT(vkCreateFence(device, &fenceCreateInfo, nullptr, &fence));

  constexpr unsigned runs = 64;
  unsigned min_time = 1 << 31;
  for(int i = 0; i < runs; ++i)
  {
    if(i != 0)
    {
      LoadDataOnGPU(numbers, dataBuffers[0]);

      if(!useSubgroups) //chain scan only
      {
        if (useSimple)
        {
          for (int j = 1; j < dataBuffers.size(); ++j)
          {
            LoadDataOnGPU({ 0 }, dataBuffers[j]);
          }
        }
        else
        {
          std::vector<unsigned> flags(numbers.size() / (WORKGROUP_SIZE * 2), 0);
          flags[0] = 1;
          LoadDataOnGPU(flags, dataBuffers[1]);
          LoadDataOnGPU({ 0 }, dataBuffers[3]);
        }
      }
    }
    auto start = std::chrono::steady_clock::now();
    {
      VkSubmitInfo submitInfo = {};
      submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
      submitInfo.commandBufferCount = 1;
      submitInfo.pCommandBuffers = &cmd;
      VK_CHECK_RESULT(vkQueueSubmit(queueCompute, 1, &submitInfo, fence));

      VK_CHECK_RESULT(vkWaitForFences(device, 1, &fence, VK_TRUE, vk_utils::DEFAULT_TIMEOUT));
    }
    auto end = std::chrono::steady_clock::now();
    auto ms = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
    min_time = ms < min_time ? ms : min_time;
    vkResetFences(device, 1, &fence);
  }
  std::cout << "prefix sum GPU (minimum over " << runs << " runs): " << min_time << " microseconds" << std::endl;

  vkDestroyFence(device, fence, nullptr);

  return OutputResults(numbers.size(), dataBuffers[0]);
}

void ComputeApplication::SubgroupPrefixSumCmd(VkCommandBuffer cmd, size_t n)
{
  const unsigned numWorkgroups = (n + WORKGROUP_SIZE - 1) / WORKGROUP_SIZE;
  std::cout << "numWorkgroups (first pass) =  " << numWorkgroups << std::endl;

  VkCommandBufferBeginInfo beginInfo = {};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
//    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
  VK_CHECK_RESULT(vkBeginCommandBuffer(cmd, &beginInfo));

  std::vector<size_t> arr_sizes;
  arr_sizes.push_back(n);
  for(size_t i = 0; i < scanDSets.size(); ++i) //
  {
    pushConstants pcData{ static_cast<uint32_t>(n) };

    vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, scanPipelineLayout, 0, 1, &scanDSets[i], 0, nullptr);
    vkCmdPushConstants(cmd, scanPipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(pcData), &pcData);
    vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, scanPipeline);
    vkCmdDispatch(cmd, (n + WORKGROUP_SIZE - 1) / WORKGROUP_SIZE, 1, 1);

    VkBufferMemoryBarrier bar1 {};
    bar1.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    bar1.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
    bar1.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    bar1.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    bar1.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    bar1.buffer = dataBuffers[i + 1];
    bar1.offset = 0;
    bar1.size = VK_WHOLE_SIZE;

    vkCmdPipelineBarrier(cmd,
                         VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                         0,
                         0, nullptr,
                         1, &bar1,
                         0, nullptr);
    n = NextBufferSizeSubgroups(n);
    arr_sizes.push_back(n);
  }

  for(int i = addDSets.size() - 2; i >= 0; --i) //
  {
    pushConstants pcData{ static_cast<uint32_t>(arr_sizes[i]) };

    vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, addPipelineLayout, 0, 1, &addDSets[i], 0, nullptr);
    vkCmdPushConstants(cmd, addPipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(pcData), &pcData);
    vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, addPipeline);
    vkCmdDispatch(cmd, (arr_sizes[i] + WORKGROUP_SIZE - 1) / WORKGROUP_SIZE, 1, 1);

    if(i != 0)
    {
      VkBufferMemoryBarrier bar1{};
      bar1.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
      bar1.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
      bar1.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
      bar1.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
      bar1.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
      bar1.buffer = dataBuffers[i - 1];
      bar1.offset = 0;
      bar1.size = VK_WHOLE_SIZE;

      vkCmdPipelineBarrier(cmd,
                           VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                           VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                           0,
                           0, nullptr,
                           1, &bar1,
                           0, nullptr);
    }
  }

  VK_CHECK_RESULT(vkEndCommandBuffer(cmd));
}

void ComputeApplication::SimplePrefixSumCmd(VkCommandBuffer cmd, size_t n)
{
  VkCommandBufferBeginInfo beginInfo = {};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
//    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
  VK_CHECK_RESULT(vkBeginCommandBuffer(cmd, &beginInfo));

  std::vector<size_t> arr_sizes;
  arr_sizes.push_back(n);
  for(size_t i = 0; i < scanDSets.size(); ++i) //
  {
    unsigned numWorkgroups = (n + (WORKGROUP_SIZE * 2) - 1) / (WORKGROUP_SIZE * 2);
//    std::cout << "numWorkgroups at pass #" << i << ": " << numWorkgroups << std::endl;
    pushConstants pcData{ static_cast<uint32_t>(WORKGROUP_SIZE * 2) };

    vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, scanPipelineLayout, 0, 1, &scanDSets[i], 0, nullptr);
    vkCmdPushConstants(cmd, scanPipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(pcData), &pcData);
    vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, scanPipeline);
    vkCmdDispatch(cmd, numWorkgroups, 1, 1);

    VkBufferMemoryBarrier bar1 {};
    bar1.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    bar1.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
    bar1.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    bar1.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    bar1.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    bar1.buffer = dataBuffers[i + 1];
    bar1.offset = 0;
    bar1.size = VK_WHOLE_SIZE;

    vkCmdPipelineBarrier(cmd,
                         VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                         0,
                         0, nullptr,
                         1, &bar1,
                         0, nullptr);
    n = NextBufferSizeSimple(n);
    arr_sizes.push_back(n);
  }

  {
    pushConstants pcData{ static_cast<uint32_t>(n) };

    vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, scanLastPipelineLayout, 0, 1, &scanLastDSet, 0, nullptr);
    vkCmdPushConstants(cmd, scanLastPipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(pcData), &pcData);
    vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, scanLastPipeline);
    vkCmdDispatch(cmd, 1, 1, 1);

    VkBufferMemoryBarrier bar1 {};
    bar1.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    bar1.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
    bar1.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    bar1.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    bar1.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    bar1.buffer = dataBuffers.back();
    bar1.offset = 0;
    bar1.size = VK_WHOLE_SIZE;

    vkCmdPipelineBarrier(cmd,
                         VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                         0,
                         0, nullptr,
                         1, &bar1,
                         0, nullptr);
  }

  for(int i = addDSets.size() - 1; i >= 0; --i) //
  {
    pushConstants pcData{ static_cast<uint32_t>(arr_sizes[i]) };

    vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, addPipelineLayout, 0, 1, &addDSets[i], 0, nullptr);
    vkCmdPushConstants(cmd, addPipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(pcData), &pcData);
    vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, addPipeline);
    vkCmdDispatch(cmd, (arr_sizes[i] + WORKGROUP_SIZE - 1) / WORKGROUP_SIZE, 1, 1);

    if(i != 0)
    {
      VkBufferMemoryBarrier bar1{};
      bar1.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
      bar1.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
      bar1.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
      bar1.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
      bar1.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
      bar1.buffer = dataBuffers[i - 1];
      bar1.offset = 0;
      bar1.size = VK_WHOLE_SIZE;

      vkCmdPipelineBarrier(cmd,
                           VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                           VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                           0,
                           0, nullptr,
                           1, &bar1,
                           0, nullptr);
    }
  }

  VK_CHECK_RESULT(vkEndCommandBuffer(cmd));
}

void ComputeApplication::StreamPrefixSumCmd(VkCommandBuffer cmd, size_t n)
{
  VkCommandBufferBeginInfo beginInfo = {};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
//    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
  VK_CHECK_RESULT(vkBeginCommandBuffer(cmd, &beginInfo));

  unsigned numWorkgroups = (n + (WORKGROUP_SIZE * 2) - 1) / (WORKGROUP_SIZE * 2);
//    std::cout << "numWorkgroups at pass #" << i << ": " << numWorkgroups << std::endl;
  pushConstants pcData{ static_cast<uint32_t>(WORKGROUP_SIZE * 2) };

  vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, scanPipelineLayout, 0, 1, &scanStreamDSet, 0, nullptr);
  vkCmdPushConstants(cmd, scanPipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(pcData), &pcData);
  vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, scanPipeline);
  vkCmdDispatch(cmd, numWorkgroups, 1, 1);

  VK_CHECK_RESULT(vkEndCommandBuffer(cmd));
}

void ComputeApplication::InitVulkan(unsigned int deviceId, bool useSubgroups)
{
  std::cout << "Init vulkan for device " << deviceId << " ... " << std::endl;

  VK_CHECK_RESULT(volkInitialize());

  std::vector<const char *> instanceExtensions{};

  VkApplicationInfo appInfo = {};
  appInfo.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  appInfo.pNext              = nullptr;
  appInfo.pApplicationName   = "vkSort";
  appInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
  appInfo.pEngineName        = "radix";
  appInfo.engineVersion      = VK_MAKE_VERSION(0, 1, 0);
  appInfo.apiVersion         = VK_MAKE_VERSION(1, 1, 0);

  instance = vk_utils::createInstance(enableValidationLayers, enabledLayers, instanceExtensions, &appInfo);
  volkLoadInstance(instance);

  if(enableValidationLayers)
  {
    vk_utils::initDebugReportCallback(instance, &debugReportCallbackFn, &debugReportCallback);
  }

  physicalDevice = vk_utils::findPhysicalDevice(instance, true, deviceId);

  if(useSubgroups)
  {
    VkPhysicalDeviceSubgroupProperties subgroupProps{};
    subgroupProps.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_PROPERTIES;

    VkPhysicalDeviceProperties2 physProps2{};
    physProps2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
    physProps2.pNext = &subgroupProps;

    vkGetPhysicalDeviceProperties2(physicalDevice, &physProps2);
    std::cout << "Subgroup size: "
              << subgroupProps.subgroupSize
              << std::endl;

    subgroupSize = subgroupProps.subgroupSize;

    std::cout << "Subgroup supported operations: \n";
    auto supportedOps = vk_utils::subgroupOperationToString(subgroupProps.supportedOperations);
    for (const auto &op : supportedOps)
    {
      std::cout << op << "; ";
    }
    std::cout << std::endl;
  }

  std::vector<const char *> deviceExtensions = { VK_KHR_SHADER_NON_SEMANTIC_INFO_EXTENSION_NAME }; // debug printf
  VkPhysicalDeviceFeatures deviceFeatures{};


  device = vk_utils::createLogicalDevice(physicalDevice, enabledLayers, deviceExtensions, deviceFeatures, queueFIDs, VK_QUEUE_TRANSFER_BIT | VK_QUEUE_COMPUTE_BIT);

  volkLoadDevice(device);

  vkGetDeviceQueue(device, queueFIDs.compute, 0, &queueCompute);
  vkGetDeviceQueue(device, queueFIDs.transfer, 0, &queueTransfer);

  VkCommandPoolCreateInfo commandPoolCreateInfo = {};
  commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
  commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
  commandPoolCreateInfo.queueFamilyIndex = queueFIDs.compute;
  VK_CHECK_RESULT(vkCreateCommandPool(device, &commandPoolCreateInfo, nullptr, &commandPoolCompute));

  commandPoolCreateInfo.queueFamilyIndex = queueFIDs.transfer;
  VK_CHECK_RESULT(vkCreateCommandPool(device, &commandPoolCreateInfo, nullptr, &commandPoolTransfer));
}

void ComputeApplication::CreateComputePipeline(VkDescriptorSetLayout a_dsLayout, VkShaderModule a_shaderModule,
                                               VkPipeline* a_pPipeline, VkPipelineLayout* a_pPipelineLayout, bool useSubgroups)
{
  unsigned consts[2] = { WORKGROUP_SIZE,  WORKGROUP_SIZE * 2};
  if(useSubgroups)
    consts[1] = WORKGROUP_SIZE / subgroupSize;

  const std::array<VkSpecializationMapEntry, 2> specMapEntries = { VkSpecializationMapEntry { 0, 0, sizeof(consts[0]) },
                                                                   VkSpecializationMapEntry { 0, sizeof(consts[0]), sizeof(consts[0]) } };
  VkSpecializationInfo specialization = { specMapEntries.size(), specMapEntries.data(), sizeof(consts[0]) * 2, consts };

  VkPipelineShaderStageCreateInfo shaderStageCreateInfo = {};
  shaderStageCreateInfo.sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  shaderStageCreateInfo.stage  = VK_SHADER_STAGE_COMPUTE_BIT;
  shaderStageCreateInfo.module = a_shaderModule;
  shaderStageCreateInfo.pSpecializationInfo = &specialization;
  shaderStageCreateInfo.pName  = "main";

  VkPushConstantRange pcRange = {};
  pcRange.size = sizeof(pushConstants);
  pcRange.offset = 0;
  pcRange.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

  VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {};
  pipelineLayoutCreateInfo.sType          = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  pipelineLayoutCreateInfo.setLayoutCount = 1;
  pipelineLayoutCreateInfo.pSetLayouts    = &a_dsLayout;
  pipelineLayoutCreateInfo.pushConstantRangeCount = 1;
  pipelineLayoutCreateInfo.pPushConstantRanges = &pcRange;
  VK_CHECK_RESULT(vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, nullptr, a_pPipelineLayout));

  VkComputePipelineCreateInfo pipelineCreateInfo = {};
  pipelineCreateInfo.sType  = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
  pipelineCreateInfo.stage  = shaderStageCreateInfo;
  pipelineCreateInfo.layout = (*a_pPipelineLayout);

  VK_CHECK_RESULT(vkCreateComputePipelines(device, VK_NULL_HANDLE, 1, &pipelineCreateInfo, nullptr, a_pPipeline));
}

void ComputeApplication::CreateResourcesSubgroups(const std::vector<uint32_t> &numbers)
{
  size_t n = numbers.size();
  size_t bufferSize = n * sizeof(numbers[0]);
  vk_utils::createBufferStaging(device, physicalDevice, bufferSize, stagingBuf, stagingMem);
  const VkBufferUsageFlags bufFlags = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

  std::vector<VkDeviceSize> bufSizes;
  while(n > 1)
  {
    bufferSize = n * sizeof(numbers[0]);
    VkBuffer buf = VK_NULL_HANDLE;
    vk_utils::createBuffer(device, bufferSize, bufFlags | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT, buf);
    dataBuffers.push_back(buf);
    bufSizes.push_back(bufferSize);
    n = NextBufferSizeSubgroups(n);
  }

  VkBuffer buf = VK_NULL_HANDLE;
  vk_utils::createBuffer(device, 1 * sizeof(numbers[0]), bufFlags | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT, buf);
  dataBuffers.push_back(buf);
  bufSizes.push_back(1 * sizeof(numbers[0]));

  bufferMemory = vk_utils::allocateAndBindWithPadding(device, physicalDevice, dataBuffers);

  LoadDataOnGPU(numbers, dataBuffers[0]);

  scanDSetLayout = vk_utils::createDescriptorSetLayout(device, { {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1}, {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1}, {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1}});
  addDSetLayout = vk_utils::createDescriptorSetLayout(device, { {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1}, {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1}});

  unsigned max_sets = (dataBuffers.size() - 1) * 2;
  descriptorPool = vk_utils::createDescriptorPool(device, { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER }, { 3 * max_sets }, max_sets);

  std::vector<std::vector<VkDescriptorBufferInfo>> bufInfosScan;
  std::vector<std::vector<VkDescriptorBufferInfo>> bufInfosAdd;
  for (std::size_t i = 0; i < dataBuffers.size() - 1; i++)
  {
    bufInfosScan.push_back({ { dataBuffers[i], 0, bufSizes[i] },
                            { dataBuffers[i], 0, bufSizes[i] },
                            { dataBuffers[i + 1], 0, bufSizes[i + 1] } });
    scanDSets.push_back(vk_utils::createDescriptorSet(device, scanDSetLayout, descriptorPool, bufInfosScan.back()));

    bufInfosAdd.push_back({ { dataBuffers[i + 1], 0, bufSizes[i + 1] },
                            { dataBuffers[i], 0, bufSizes[i] } });
    addDSets.push_back(vk_utils::createDescriptorSet(device, addDSetLayout, descriptorPool, bufInfosAdd.back()));
  }


  {
    auto shaderCode = vk_utils::readSPVFile("../shaders/scan.comp.spv");
    scanShaderModule = vk_utils::createShaderModule(device, shaderCode);
  }

  {
    auto shaderCode = vk_utils::readSPVFile("../shaders/add_partial.comp.spv");
    addShaderModule = vk_utils::createShaderModule(device, shaderCode);
  }

  CreateComputePipeline(scanDSetLayout, scanShaderModule, &scanPipeline, &scanPipelineLayout, true);
  CreateComputePipeline(addDSetLayout, addShaderModule, &addPipeline, &addPipelineLayout, true);
}

void ComputeApplication::CreateResourcesSimple(const std::vector<uint32_t> &numbers)
{
  size_t n = numbers.size();
  size_t bufferSize = n * sizeof(numbers[0]);
  vk_utils::createBufferStaging(device, physicalDevice, bufferSize, stagingBuf, stagingMem);
  const VkBufferUsageFlags bufFlags = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

  std::vector<VkDeviceSize> bufSizes;
  while(n > WORKGROUP_SIZE * 2)
  {
    bufferSize = n * sizeof(numbers[0]);
    VkBuffer buf = VK_NULL_HANDLE;
    vk_utils::createBuffer(device, bufferSize, bufFlags | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT, buf);
    dataBuffers.push_back(buf);
    bufSizes.push_back(bufferSize);
    n = NextBufferSizeSimple(n);
  }

  VkBuffer buf = VK_NULL_HANDLE;
  vk_utils::createBuffer(device, n * sizeof(numbers[0]), bufFlags | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT, buf);
  dataBuffers.push_back(buf);
  bufSizes.push_back(n * sizeof(numbers[0]));

  bufferMemory = vk_utils::allocateAndBindWithPadding(device, physicalDevice, dataBuffers);

  LoadDataOnGPU(numbers, dataBuffers[0]);

  scanDSetLayout     = vk_utils::createDescriptorSetLayout(device, { {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1},
                                                                     {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1}});
  scanLastDSetLayout = vk_utils::createDescriptorSetLayout(device, { {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1} });
  addDSetLayout      = vk_utils::createDescriptorSetLayout(device, { {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1},
                                                                     {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1}});

  unsigned max_sets = (dataBuffers.size() - 1) * 2 + 1;
  descriptorPool = vk_utils::createDescriptorPool(device, { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER }, { 2 * max_sets + 1 }, max_sets);

  std::vector<std::vector<VkDescriptorBufferInfo>> bufInfosScan;
  std::vector<std::vector<VkDescriptorBufferInfo>> bufInfosAdd;
  for (std::size_t i = 0; i < dataBuffers.size() - 1; i++)
  {
    bufInfosScan.push_back({ { dataBuffers[i], 0, bufSizes[i] },
                            { dataBuffers[i + 1], 0, bufSizes[i + 1] } });
    scanDSets.push_back(vk_utils::createDescriptorSet(device, scanDSetLayout, descriptorPool, bufInfosScan.back()));

    bufInfosAdd.push_back({ { dataBuffers[i + 1], 0, bufSizes[i + 1] },
                            { dataBuffers[i], 0, bufSizes[i] } });
    addDSets.push_back(vk_utils::createDescriptorSet(device, addDSetLayout, descriptorPool, bufInfosAdd.back()));
  }
  std::vector<VkDescriptorBufferInfo> bufInfoLastScan = {{ dataBuffers.back(), 0, bufSizes.back() }};
  scanLastDSet = vk_utils::createDescriptorSet(device, scanLastDSetLayout, descriptorPool, bufInfoLastScan);

  {
    auto shaderCode = vk_utils::readSPVFile("../shaders/scan_nosub_opt.comp.spv");
    scanShaderModule = vk_utils::createShaderModule(device, shaderCode);
  }
  {
    auto shaderCode = vk_utils::readSPVFile("../shaders/scan_nosub_last_opt.comp.spv");
    scanLastShaderModule = vk_utils::createShaderModule(device, shaderCode);
  }
  {
    auto shaderCode = vk_utils::readSPVFile("../shaders/add_partial_v2.comp.spv");
    addShaderModule = vk_utils::createShaderModule(device, shaderCode);
  }

  CreateComputePipeline(scanDSetLayout, scanShaderModule, &scanPipeline, &scanPipelineLayout, false);
  CreateComputePipeline(scanLastDSetLayout, scanLastShaderModule, &scanLastPipeline, &scanLastPipelineLayout, false);
  CreateComputePipeline(addDSetLayout, addShaderModule, &addPipeline, &addPipelineLayout, false);
}

void ComputeApplication::CreateResourcesStream(const std::vector<uint32_t> &numbers)
{
  size_t n = numbers.size();
  size_t bufferSize = n * sizeof(numbers[0]);
  vk_utils::createBufferStaging(device, physicalDevice, bufferSize, stagingBuf, stagingMem);
  const VkBufferUsageFlags bufFlags = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

  std::vector<VkDeviceSize> bufSizes;

  // main buffer
  {
    bufferSize = n * sizeof(numbers[0]);
    VkBuffer buf = VK_NULL_HANDLE;
    vk_utils::createBuffer(device, bufferSize, bufFlags | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT, buf);
    dataBuffers.push_back(buf);
    bufSizes.push_back(bufferSize);
  }

  const unsigned numBlocks = n / (WORKGROUP_SIZE * 2);
  std::vector<unsigned> flags(numBlocks, 0);
  flags[0] = 1;
  // flags buffer
  {
    VkBuffer buf = VK_NULL_HANDLE;
    vk_utils::createBuffer(device, numBlocks * sizeof(flags[0]), bufFlags | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT, buf);
    dataBuffers.push_back(buf);
    bufSizes.push_back(numBlocks * sizeof(flags[0]));
  }

  // scan value propagation buffer
  {
    VkBuffer buf = VK_NULL_HANDLE;
    vk_utils::createBuffer(device, numBlocks * sizeof(numbers[0]), bufFlags | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT, buf);
    dataBuffers.push_back(buf);
    bufSizes.push_back(numBlocks * sizeof(numbers[0]));
  }

  // block number sync buf
  {
    VkBuffer buf = VK_NULL_HANDLE;
    vk_utils::createBuffer(device, sizeof(unsigned), bufFlags | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT, buf);
    dataBuffers.push_back(buf);
    bufSizes.push_back(sizeof(unsigned));
  }

  bufferMemory = vk_utils::allocateAndBindWithPadding(device, physicalDevice, dataBuffers);

  LoadDataOnGPU(numbers, dataBuffers[0]);
  LoadDataOnGPU(flags, dataBuffers[1]);

  scanStreamDSetLayout = vk_utils::createDescriptorSetLayout(device, { {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1},
                                                                       {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1},
                                                                       {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1},
                                                                       {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1}});

  unsigned max_sets = 1;
  descriptorPool = vk_utils::createDescriptorPool(device, { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER }, { 4 }, max_sets);

  std::vector<VkDescriptorBufferInfo> bufInfosScan;
  for (std::size_t i = 0; i < dataBuffers.size(); i++)
  {
    bufInfosScan.push_back({ dataBuffers[i], 0, bufSizes[i] });
  }
  scanStreamDSet = vk_utils::createDescriptorSet(device, scanStreamDSetLayout, descriptorPool, bufInfosScan);

  {
    auto shaderCode = vk_utils::readSPVFile("../shaders/scan_stream.comp.spv");
    scanShaderModule = vk_utils::createShaderModule(device, shaderCode);
  }

  CreateComputePipeline(scanStreamDSetLayout, scanShaderModule, &scanPipeline, &scanPipelineLayout, false);
}

void ComputeApplication::LoadDataOnGPU(const std::vector<uint32_t> &numbers, VkBuffer dataInBuffer)
{
  size_t bufferSize = numbers.size() * sizeof(numbers[0]);
  void* mappedMemory = nullptr;

  {
    vkMapMemory(device, stagingMem, 0, bufferSize, 0, &mappedMemory);
    memcpy(mappedMemory, numbers.data(), bufferSize);
    vkUnmapMemory(device, stagingMem);
  }

  VkCommandBuffer copyBuf;
  VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
  commandBufferAllocateInfo.sType       = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  commandBufferAllocateInfo.commandPool = commandPoolTransfer;
  commandBufferAllocateInfo.level       = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  commandBufferAllocateInfo.commandBufferCount = 1;
  VK_CHECK_RESULT(vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &copyBuf));

  VkCommandBufferBeginInfo beginInfo = {};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

  vkBeginCommandBuffer(copyBuf, &beginInfo);
  VkBufferCopy region0 = {};
  region0.srcOffset    = 0;
  region0.dstOffset    = 0;
  region0.size         = numbers.size() * sizeof(numbers[0]);
  vkCmdCopyBuffer(copyBuf, stagingBuf, dataInBuffer, 1, &region0);
  vkEndCommandBuffer(copyBuf);

  VkSubmitInfo submitInfo       = {};
  submitInfo.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers    = &copyBuf;

  VkFence fence;
  VkFenceCreateInfo fenceCreateInfo = {};
  fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fenceCreateInfo.flags = 0;

  VK_CHECK_RESULT(vkCreateFence(device, &fenceCreateInfo, nullptr, &fence));
  VK_CHECK_RESULT(vkQueueSubmit(queueTransfer, 1, &submitInfo, fence));
  VK_CHECK_RESULT(vkWaitForFences(device, 1, &fence, VK_TRUE, vk_utils::DEFAULT_TIMEOUT));

  vkDestroyFence(device, fence, nullptr);
}

std::vector<uint32_t> ComputeApplication::OutputResults(size_t arr_size, VkBuffer dataOutBuffer)
{
  std::vector<uint32_t> sorted_arr(arr_size);

  VkCommandBuffer copyBuf;
  VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
  commandBufferAllocateInfo.sType       = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  commandBufferAllocateInfo.commandPool = commandPoolTransfer;
  commandBufferAllocateInfo.level       = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  commandBufferAllocateInfo.commandBufferCount = 1;
  VK_CHECK_RESULT(vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &copyBuf));

  VkCommandBufferBeginInfo beginInfo = {};
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

  vkBeginCommandBuffer(copyBuf, &beginInfo);
  VkBufferCopy region0 = {};
  region0.srcOffset    = 0;
  region0.dstOffset    = 0;
  region0.size         = sorted_arr.size() * sizeof(sorted_arr[0]);
  vkCmdCopyBuffer(copyBuf, dataOutBuffer, stagingBuf, 1, &region0);
  vkEndCommandBuffer(copyBuf);

  VkSubmitInfo submitInfo       = {};
  submitInfo.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers    = &copyBuf;

  VkFence fence;
  VkFenceCreateInfo fenceCreateInfo = {};
  fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fenceCreateInfo.flags = 0;

  VK_CHECK_RESULT(vkCreateFence(device, &fenceCreateInfo, nullptr, &fence));
  VK_CHECK_RESULT(vkQueueSubmit(queueTransfer, 1, &submitInfo, fence));
  VK_CHECK_RESULT(vkWaitForFences(device, 1, &fence, VK_TRUE, vk_utils::DEFAULT_TIMEOUT));

  vkDestroyFence(device, fence, nullptr);

  void* mappedMemory = nullptr;
  {
    vkMapMemory(device, stagingMem, 0, region0.size, 0, &mappedMemory);
    memcpy(sorted_arr.data(), mappedMemory, region0.size);
    vkUnmapMemory(device, stagingMem);
  }

  return sorted_arr;
}


void ComputeApplication::Cleanup()
{
  if (enableValidationLayers)
  {
    auto func = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
    if (func == nullptr)
    {
      throw std::runtime_error("Could not load vkDestroyDebugReportCallbackEXT");
    }
    func(instance, debugReportCallback, nullptr);
  }

  {
    vkFreeMemory(device, bufferMemory, nullptr);
    for(auto& buf : dataBuffers)
      vkDestroyBuffer(device, buf, nullptr);
  }

  {
    vkFreeMemory(device, stagingMem, nullptr);
    vkDestroyBuffer(device, stagingBuf, nullptr);
  }

  {
    vkDestroyDescriptorPool(device, descriptorPool, nullptr);
    vkDestroyDescriptorSetLayout(device, scanDSetLayout, nullptr);
    vkDestroyDescriptorSetLayout(device, scanLastDSetLayout, nullptr);
    vkDestroyDescriptorSetLayout(device, addDSetLayout, nullptr);
    vkDestroyDescriptorSetLayout(device, scanStreamDSetLayout, nullptr);
  }

  {
    vkDestroyShaderModule(device, scanShaderModule, nullptr);
    vkDestroyPipelineLayout(device, scanPipelineLayout, nullptr);
    vkDestroyPipeline(device, scanPipeline, nullptr);

    vkDestroyShaderModule(device, scanLastShaderModule, nullptr);
    vkDestroyPipelineLayout(device, scanLastPipelineLayout, nullptr);
    vkDestroyPipeline(device, scanLastPipeline, nullptr);

    vkDestroyShaderModule(device, addShaderModule, nullptr);
    vkDestroyPipelineLayout(device, addPipelineLayout, nullptr);
    vkDestroyPipeline(device, addPipeline, nullptr);
  }

  {
    vkDestroyCommandPool(device, commandPoolCompute, nullptr);
    vkDestroyCommandPool(device, commandPoolTransfer, nullptr);
  }

  vkDestroyDevice(device, nullptr);
  vkDestroyInstance(instance, nullptr);
}