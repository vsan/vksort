#ifndef VK_SORT_VK_RADIX_H
#define VK_SORT_VK_RADIX_H

#include "vk_utils.h"
#include "vk_buffers.h"
#include "../shaders/shaderCommon.h"

class ComputeApplication
{
private:
  VkInstance instance = VK_NULL_HANDLE;
  VkDebugReportCallbackEXT debugReportCallback = VK_NULL_HANDLE;
  VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
  VkDevice device = VK_NULL_HANDLE;

  std::vector<const char *> enabledLayers = {"VK_LAYER_KHRONOS_validation",  "VK_LAYER_LUNARG_monitor"};

  VkPipeline scanPipeline = VK_NULL_HANDLE;
  VkPipelineLayout scanPipelineLayout = VK_NULL_HANDLE;
  VkShaderModule scanShaderModule = VK_NULL_HANDLE;

  VkPipeline scanLastPipeline = VK_NULL_HANDLE;
  VkPipelineLayout scanLastPipelineLayout = VK_NULL_HANDLE;
  VkShaderModule scanLastShaderModule = VK_NULL_HANDLE;

  VkPipeline addPipeline = VK_NULL_HANDLE;
  VkPipelineLayout addPipelineLayout = VK_NULL_HANDLE;
  VkShaderModule addShaderModule = VK_NULL_HANDLE;

  VkCommandPool commandPoolCompute = VK_NULL_HANDLE;
  VkCommandPool commandPoolTransfer = VK_NULL_HANDLE;

  VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
  std::vector<VkDescriptorSet> scanDSets;
  VkDescriptorSetLayout scanDSetLayout = VK_NULL_HANDLE;
  VkDescriptorSet scanLastDSet = VK_NULL_HANDLE;
  VkDescriptorSetLayout scanLastDSetLayout = VK_NULL_HANDLE;
  std::vector<VkDescriptorSet> addDSets;
  VkDescriptorSetLayout addDSetLayout = VK_NULL_HANDLE;

  VkDescriptorSet scanStreamDSet = VK_NULL_HANDLE;
  VkDescriptorSetLayout scanStreamDSetLayout = VK_NULL_HANDLE;

  vk_utils::QueueFID_T queueFIDs {};
  VkQueue queueCompute = VK_NULL_HANDLE;
  VkQueue queueTransfer = VK_NULL_HANDLE;

  std::vector<VkBuffer> dataBuffers;
  VkDeviceMemory bufferMemory = VK_NULL_HANDLE;

  VkBuffer stagingBuf = VK_NULL_HANDLE;
  VkDeviceMemory stagingMem = VK_NULL_HANDLE;

  struct pushConstants {
    uint32_t array_size;
  };

  unsigned subgroupSize = 32;

  void InitVulkan(unsigned deviceId, bool useSubgroups);

  void SubgroupPrefixSumCmd(VkCommandBuffer cmd, size_t n);
  void SimplePrefixSumCmd(VkCommandBuffer cmd, size_t n);
  void StreamPrefixSumCmd(VkCommandBuffer cmd, size_t n);

  void CreateComputePipeline(VkDescriptorSetLayout a_dsLayout, VkShaderModule a_shaderModule,
                             VkPipeline* a_pPipeline, VkPipelineLayout* a_pPipelineLayout, bool useSubgroups);

  void CreateResourcesSubgroups(const std::vector<uint32_t> &numbers);
  void CreateResourcesSimple(const std::vector<uint32_t> &numbers);
  void CreateResourcesStream(const std::vector<uint32_t> &numbers);
  void LoadDataOnGPU(const std::vector<uint32_t> &numbers, VkBuffer dataInBuffer);
  std::vector<uint32_t> OutputResults(size_t arr_size, VkBuffer dataOutBuffer);

  void Cleanup();

  static size_t NextBufferSizeSubgroups(size_t data_size)
  {
    return (data_size + WORKGROUP_SIZE) / WORKGROUP_SIZE;
  }

  static size_t NextBufferSizeSimple(size_t data_size)
  {
    return data_size / (WORKGROUP_SIZE * 2);
  }

#ifdef NDEBUG
  bool enableValidationLayers = false;
#else
  bool enableValidationLayers = true;
#endif
  static VKAPI_ATTR VkBool32 VKAPI_CALL debugReportCallbackFn(
      VkDebugReportFlagsEXT                       flags,
      VkDebugReportObjectTypeEXT                  objectType,
      uint64_t                                    object,
      size_t                                      location,
      int32_t                                     messageCode,
      const char*                                 pLayerPrefix,
      const char*                                 pMessage,
      void*                                       pUserData)
  {
    printf("Debug Report: %s: %s\n", pLayerPrefix, pMessage);
    return VK_FALSE;
  }
public:
  ComputeApplication() = default;
  ComputeApplication(const ComputeApplication&) = delete;
  ComputeApplication operator=(ComputeApplication) = delete;
  ComputeApplication(ComputeApplication&&) = delete;
  ~ComputeApplication()
  {
    Cleanup();
  }

  std::vector<uint32_t> Run(const std::vector<uint32_t> &numbers, unsigned deviceId = 0, bool useSubgroups = true,
                            bool useSimple = true);

};

#endif //VK_SORT_VK_RADIX_H
