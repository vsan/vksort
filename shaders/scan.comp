#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_GOOGLE_include_directive : require
#extension GL_KHR_shader_subgroup_arithmetic : enable

#ifdef DEBUG
#extension GL_EXT_debug_printf : enable
#endif

#include "shaderCommon.h"

layout (local_size_x_id = 0) in;
layout (constant_id = 2) const int subGroupSize = 32;


layout(std430, binding = 0) buffer bufIn
{
   uint in_arr[];
};

layout(std430, binding = 1) buffer bufOut
{
  uint out_arr[];
};

layout(std430, binding = 2) buffer bufTmp
{
  uint partial_results[];
};


layout(push_constant) uniform PushConsts
{
    int N;
} consts;

shared uint sdata[subGroupSize];

void main()
{
  const uint tid = gl_GlobalInvocationID.x;
  uint sum = 0u;

  if(tid < consts.N)
  {
    sum = in_arr[tid];
  }

  sum = subgroupInclusiveAdd(sum);

  // last thread in each subgroup writes sum over subgroup into share mem
  if(gl_SubgroupInvocationID == gl_SubgroupSize  - 1)
  {
    sdata[gl_SubgroupID] = sum;
  }

  memoryBarrierShared();
  barrier();

  // first subgroup calculates prefix sum over shared mem, i.e. all subgroups in a workgroup
  if(gl_SubgroupID == 0)
  {
    uint warp_sum = gl_SubgroupInvocationID < gl_NumSubgroups ? sdata[gl_SubgroupInvocationID] : 0;
//#ifdef DEBUG
//    debugPrintfEXT("gl_NumSubgroups = %d gl_SubgroupInvocationID = %d, warp_sum = %d",
//gl_NumSubgroups, gl_SubgroupInvocationID, warp_sum);
//#endif
    warp_sum = subgroupInclusiveAdd(warp_sum);
    sdata[gl_SubgroupInvocationID] = warp_sum;
//#ifdef DEBUG
//    debugPrintfEXT("gl_SubgroupInvocationID = %d, warp_sum = %d",
//                    gl_SubgroupInvocationID, warp_sum);
//#endif
  }

  memoryBarrierShared();
  barrier();

  uint block_sum = 0;
  // all subgroups (except first) take block_sum from shared mem
  // and add it to per thread sum to calculate prefix sum over all workgroup
  if(gl_SubgroupID > 0)
  {
    block_sum = sdata[gl_SubgroupID - 1];
#ifdef DEBUG
    if(gl_WorkGroupID.x == 0)
      debugPrintfEXT("gl_NumSubgroups = %d, gl_SubgroupID = %d, block_sum = %d",
                      gl_NumSubgroups, gl_SubgroupID, block_sum);
#endif
  }
  sum += block_sum;

  if(tid < consts.N)
  {
    out_arr[tid] = sum;
  }

  // last thread in workgroup outputs sum over workgroup to partial_results
  if(gl_LocalInvocationID.x == gl_WorkGroupSize.x - 1)
  {
    partial_results[gl_WorkGroupID.x] = sum;

//#ifdef DEBUG
//    debugPrintfEXT("gl_WorkGroupID.x = %d, partial_results[gl_WorkGroupID.x] = %d",
//                   gl_WorkGroupID.x, partial_results[gl_WorkGroupID.x]);
//#endif
  }
}