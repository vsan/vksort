#ifndef VK_SHADERCOMMON_H
#define VK_SHADERCOMMON_H

#define WORKGROUP_SIZE 256

#define NUM_BANKS 32 // Nvidia
#define LOG_NUM_BANKS 5 // log2(NUM_BANKS)
//#define CONFLICT_FREE_OFFSET(n) ((n) >> NUM_BANKS + (n) >> (2 * LOG_NUM_BANKS))
#define CONFLICT_FREE_OFFSET(n) ((n) >> LOG_NUM_BANKS)

#endif //VK_SHADERCOMMON_H
