glslangValidator -V shader.comp -o comp.spv --D GLSL
glslangValidator --target-env vulkan1.1 -V scan.comp -o scan.comp.spv --D GLSL
glslangValidator --target-env vulkan1.1 -V add_partial.comp -o add_partial.comp.spv --D GLSL
glslangValidator -V scan_nosub.comp -o scan_nosub.comp.spv --D GLSL
glslangValidator -V scan_nosub_opt.comp -o scan_nosub_opt.comp.spv --D GLSL
glslangValidator -V scan_nosub.comp -o scan_nosub_last.comp.spv --D GLSL --D LAST_PASS
glslangValidator -V scan_nosub_opt.comp -o scan_nosub_last_opt.comp.spv --D GLSL --D LAST_PASS
glslangValidator -V add_partial_v2.comp -o add_partial_v2.comp.spv --D GLSL
glslangValidator -V scan_stream.comp -o scan_stream.comp.spv --D GLSL