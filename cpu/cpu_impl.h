#ifndef VK_SORT_CPU_IMPL_H
#define VK_SORT_CPU_IMPL_H

#include <vector>
#include <array>
#include <numeric>
#include <cmath>
#include <functional>
#include <iostream>
#include <climits>

//#define VERBOSE

template <typename T>
T default_key(T val)
{
  return val;
}

template <typename T>
std::vector<std::function<T(T)>> gen_keys(uint32_t r, uint32_t n_iters)
{
  std::vector<std::function<T(T)>> res;
  const uint32_t base_mask = (1u << r) - 1;
  for(auto i = 0; i < n_iters; ++i)
  {
    const auto shift = r * i;
    res.emplace_back([shift, base_mask](uint32_t val){
      auto mask = base_mask << shift;
      auto v = val & mask;
      auto res = v >> shift;
      return res;
    });
  }

  return res;
}

template <typename T, typename U = decltype(default_key<typename T::value_type>)>
void counting_sort(const T &in_seq, T &out_seq, size_t k, U get_key = default_key<typename T::value_type>)
{
  std::vector<typename T::value_type> temp(k);

  for(auto j = 0; j < in_seq.size(); ++j)
    temp[get_key(in_seq[j])] += 1;

  for(auto i = 1u; i < k; ++i)
    temp[i] += temp[i - 1];
//  std::inclusive_scan(temp.begin(), temp.end(), temp.begin());

  auto j = in_seq.size();
  do
  {
    j--;
    auto key = get_key(in_seq[j]);
    out_seq[temp[key] - 1] = in_seq[j];
    temp[key] -= 1;
  } while(j != 0);
}

template <typename T>
void radix_sort(T &seq)
{
  constexpr uint16_t nBytes = sizeof(typename T::value_type);
  constexpr uint16_t nBits  = nBytes * CHAR_BIT;

  const uint32_t lgN = std::floor(std::log2(seq.size()));
  uint32_t r = nBits;
  if(nBits >= lgN)
    r = lgN;

  const uint64_t k = 1u << r;
  const uint32_t numIters = (nBits + r - 1) / r;

#ifdef VERBOSE
  std::cout << "CPU Radix sort ---------\n";
  std::cout << "\t array size = " << seq.size() << "\n";
  std::cout << "\t nBits = " << nBits << "\n";
  std::cout << "\t log2(array size) = " << lgN << "\n";
  std::cout << "\t r = " << r << "\n";
  std::cout << "\t k (tmp array size) = " << k << "\n";
  std::cout << "\t sort iterations = " << numIters << std::endl;
#endif

  auto sorted_seq = seq;
  auto& A = seq;
  auto& B = sorted_seq;
  auto keys = gen_keys<typename T::value_type>(r, numIters);
  for(size_t i = 0; i < numIters; ++i)
  {
    counting_sort(A, B, k, keys[i]);
    std::swap(A, B);
  }
}

template <typename T>
void print_vec(std::ostream& os, const std::vector<T> &v)
{
  for(const auto& el : v)
  {
    os << el << "; ";
  }
}

#endif //VK_SORT_CPU_IMPL_H
