#include <iostream>
#include <vector>
#include <random>
#include <fstream>

#include "cpu/cpu_impl.h"
#include "gpu/vk_radix.h"
#include "tests.h"

void check()
{
  std::vector<uint32_t> data = {10, 15, 133, 1, 6, 72, 800, 900, 80, 12, 150, 21, 33, 400, 80, 82};

  radix_sort(data);
  print_vec(std::cout, data);
  std::cout << std::endl;
}


int main()
{
//  run_all_tests();
//  check();

//  std::vector<uint32_t> data = {10, 15, 133, 1, 6, 72, 800, 900, 80, 12, 150, 21, 33, 400, 80, 82};
  constexpr unsigned num_elems = 1024 * 1024 * 16;
//  constexpr unsigned num_elems = 1024;
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(0, 65536);
  std::vector<uint32_t> data(num_elems);
//  std::iota(data.begin(), data.end(), 0);
  for(auto& d : data)
    d = distrib(gen);

  unsigned deviceId = 0;

  ComputeApplication app;
  auto gpu_sorted_arr = app.Run(data, deviceId, false, false);


//  std::cout << "\t[initial data][512]  = " << data[256] << std::endl;
  for(size_t i = 1; i < data.size(); ++i)
  {
    data[i] += data[i - 1];
  }

  bool flag = true;
  size_t idx = 0;
  for(size_t i = 0; i < data.size(); ++i)
  {
    if(data[i] != gpu_sorted_arr[i])
    {
      std::cout << "CPU and GPU results differ at idx = " << i << std::endl;
      std::cout << "\t[CPU]  = " << data[i] << std::endl;
      std::cout << "\t[GPU]  = " << gpu_sorted_arr[i] << std::endl;
      idx = i;
      flag = false;
      break;
    }
  }

  if(flag)
  {
    std::cout << "CPU and GPU results match!" << std::endl;
  }
  else
  {
    std::ofstream cpu_results("cpu_res.txt", std::ios::trunc);
    for(const auto& el : data)
    {
      cpu_results << el << "\n";
    }
    cpu_results.close();

    std::ofstream gpu_results("gpu_res.txt", std::ios::trunc);
    for(const auto& el : gpu_sorted_arr)
    {
      gpu_results << el << "\n";
    }
    gpu_results.close();
  }



  return 0;
}
